package generator

import (
	"fmt"
	"strings"
)

func UpdateQuestion() {
	questionId = 0
	emotionSelectHtml := generateQuestions(emotion)
	emotionHTML := ""
	emotionHTML += header + "\n"
	emotionHTML += title + "\n"
	emotionHTML += style + "\n"
	emotionHTML += body + "\n"
	emotionHTML += video + "\n"
	emotionHTML += emotionSelectHtml + "\n"
	emotionHTML += endQuestion + "\n"
	emotionHTML += initScript + "\n"
	emotionHTML += selectionScript + "\n"
	emotionHTML += endHTML + "\n"
	//err := ioutil.WriteFile("server/static/emotion.html", []byte(emotionHTML), 0644)
	moodSelectHtml := generateQuestions(mood)
	moodHTML := ""
	moodHTML += header + "\n"
	moodHTML += title + "\n"
	moodHTML += style + "\n"
	moodHTML += body + "\n"
	moodHTML += video + "\n"
	moodHTML += moodSelectHtml + "\n"
	moodHTML += endQuestion + "\n"
	moodHTML += initScript + "\n"
	moodHTML += selectionScript + "\n"
	moodHTML += endHTML + "\n"
	//err = ioutil.WriteFile("server/static/mood.html", []byte(moodHTML), 0644)
	//if err != nil {
	//}
	demographicsSelectHtml := generateQuestions(demographics)
	demographicsHTML := ""
	demographicsHTML += header + "\n"
	demographicsHTML += title + "\n"
	demographicsHTML += style + "\n"
	demographicsHTML += body + "\n"
	demographicsHTML += demographicsSelectHtml + "\n"
	demographicsHTML += endHTML + "\n"
	//fmt.Println(demographicsHTML)
}

var emotion = []Question{
	{
		"Were you able to play and watch the video?",
		[]Option{
			{"Yes", TypeRadio},
			{"No", TypeRadio},
		},
	},
	{
		"How do you think Miro felt in this video? (select multiple if you cannot decide on one. If you select ‘not sure’, you must select another answer as well)",
		[]Option{
			{"Happy", TypeCheckbox},
			{"Sad", TypeCheckbox},
			{"Excited", TypeCheckbox},
			{"Fearful", TypeCheckbox},
			{"Angry", TypeCheckbox},
			{"Disgusted", TypeCheckbox},
			{"Neutral", TypeCheckbox},
			{"Not Sure", TypeCheckbox},
		},
	},
	{
		"Briefly explain your choice (e.g., I thought Miro was --- because ---). You can also say ‘Not sure’ if you cannot explain your choice",
		[]Option{
			{"", TypeText},
		},
	},
}

var mood = []Question{
	{
		"Were you able to play and watch the video?",
		[]Option{
			{"Yes", TypeRadio},
			{"No", TypeRadio},
		},
	},
	{
		"What was the mood of Miro in this video? (select multiple if you cannot decide on one. If you select ‘not sure’, you must select another answer as well)",
		[]Option{
			{"Bored", TypeCheckbox},
			{"Annoyed", TypeCheckbox},
			{"Relax/Calm", TypeCheckbox},
			{"Cheerful", TypeCheckbox},
			{"Tired", TypeCheckbox},
			{"Comfortable", TypeCheckbox},
			{"Not Sure", TypeCheckbox},
		},
	},
	{
		"Briefly explain your choice (e.g., I thought Miro was --- because ---). You can also say ‘Not sure’ if you cannot explain your choice",
		[]Option{
			{"", TypeText},
		},
	},
}

var demographics = []Question {
	{
		"Ethnicity/culture that you associate yourself with (write NA if you prefer not to share. Write all if there are multiple.):",
		[]Option{
			{"", TypeText},
		},
	},
	{
		"Which animal/animals did Miro remind you of? (there is no right or wrong answer to this question, we want to know what you thought it was)",
		[]Option{
			{"", TypeText},
		},
	},
	{
		"I have a pet.",
		[]Option{
			{"Yes", TypeRadio},
			{"No", TypeRadio},
		},
	},
	{
		"What is your pet?",
		[]Option{
			{"", TypeText},
		},
	},
	{
		"I have a close friend or family member who has a pet.",
		[]Option{
			{"Yes", TypeRadio},
			{"No", TypeRadio},
		},
	},
	{
		"What is their pet?",
		[]Option{
			{"", TypeText},
		},
	},
}

var header = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>`

var title = `	<title>Miro Survey</title>`

var style = `	<style>
        .checkbox-custom, .radio-custom {
            opacity: 0;
            position: absolute;
        }

        .checkbox-custom, .checkbox-custom-label, .radio-custom, .radio-custom-label {
            display: inline-block;
            vertical-align: middle;
            margin: 5px;
            cursor: pointer;
        }

        .checkbox-custom-label, .radio-custom-label {
            position: relative;
        }

        .checkbox-custom + .checkbox-custom-label:before, .radio-custom + .radio-custom-label:before {
            content: '';
            background: #fff;
            border: 2px solid #000;
            display: inline-block;
            vertical-align: middle;
            width: 20px;
            height: 20px;
            /*padding: 2px;*/
            margin-right: 10px;
            text-align: center;
        }

        .checkbox-custom:checked + .checkbox-custom-label:before {
            background: rebeccapurple;
            /*box-shadow: inset 0px 0px 0px 4px #fff;*/
        }

        .radio-custom + .radio-custom-label:before {
            border-radius: 50%;
        }

        .radio-custom:checked + .radio-custom-label:before {
            background: #000;
            /*box-shadow: inset 0px 0px 0px 4px #fff;*/
        }


        .checkbox-custom:focus + .checkbox-custom-label, .radio-custom:focus + .radio-custom-label {
            /*outline: 1px solid #ddd; !* focus style *!*/
        }

    </style>
</head>`

var body = `<body onload="init()" style="background-color: #234470">
<div class="container" style="background-color: #EFF8FD; min-height: 100vh">
    <div class="row">
        <div class="col-lg-2 col-md-2"></div>
        <div class="col-lg-8 col-md-8" style="text-align:center">
			<h2>Miro Survey</h2>
        </div>
        <div class="col-lg-2 col-md-2"></div>
    </div>`

var video = `	<div class="row">
        <div class="col-lg-2 col-md-2"></div>
        <div class="col-lg-8 col-md-8" style="margin:auto;padding:0">
            <h3> Please watch the video and answer the following questions: </h3>
			<video id="video" width="300" height="540">
				<source src="error.mp4" type="video/mp4">
				Your browser does not support the video tag.
			</video>
        </div>
    </div>`

var questionId int

func generateQuestions(questions []Question) string {
	questionHTMLs := make([]string, 0)
	for _, question := range questions {
		questionHTMLs = append(questionHTMLs, generateQuestion(question))
	}
	return strings.Join(questionHTMLs, "\n")
}

func generateQuestion(question Question) string {
	questionId ++
	curQuestionId := questionId
	frame := fmt.Sprintf("%d_frame", curQuestionId)
	options := make([]string, 0)
	for optionId, option := range question.Options {
		options = append(options, generateOption(option, curQuestionId, optionId + 1))
	}
	return `	<div class="row">
        <div class="col-lg-2 col-md-2"></div>
        <div class="col-lg-8 col-md-8" id="` + frame + `">
            <div class="row">
                <h4><b>` + question.Body + `</b></h4>
            </div>
` + strings.Join(options, "") + `		</div>
    </div>
`
}

func generateOption(option Option, questionId, optionId int) string {
	optionType := optionTypes[option.Type]
	questionIdText := fmt.Sprintf("%d", questionId)
	optionIdText := fmt.Sprintf("%d_%d", questionId, optionId)
	if option.Type == TypeText {
		return `			<div class="row">
                <textarea rows="10" name="` + questionIdText + `" id="` + optionIdText + `" style="width:100%" maxlength="1024"></textarea>
                <label for="` + optionIdText + `" class="` + optionType + `-custom-label">` + option.Body + `</label>
            </div>
`
	}
	return `			<div class="row">
                <input type="` + optionType + `" class="` + optionType + `-custom" name="` + questionIdText + `" id="` + optionIdText + `" onclick="selected(this.id)" />
                <label for="` + optionIdText + `" class="` + optionType + `-custom-label">` + option.Body + `</label>
            </div>
`
}

var endQuestion = `    <div class="row" style="height: 50px"></div>
    <div style="text-align:center">
        <input class="btn btn-success" type="button" value="Next" onclick="checkComplete()" style="border: 2px solid #000; color: #000; width:70px; height:50px; font-weight: bold; font-size: 20px">
        <input class="btn btn-danger" type="button" value="Exit" onclick="toThankYou()" style="border: 2px solid #000; color: #000; width:70px; height:50px; font-weight: bold; font-size: 20px">
    </div>
    <div class="row" style="height:100px"></div>
</div>
<script>
	let split = window.location.href.split("/");
	let domain = split.slice(0, split.length - 1).join("/");
	let subject = "error";
	let session_id = "error";`

var initScript = `
	function init() {
		let url = new URL(window.location.href);
        session_id = url.searchParams.get("session_id");
		if (session_id == null || session_id === "" || session_id === "error") window.location.replace(domain + "/" + "error.html");

		let xhttp = new XMLHttpRequest();
		xhttp.open("GET", domain + "/subject?session_id=" + session_id);
		xhttp.onreadystatechange = function() {
			if (xhttp.readyState === 4) {
				subject = xhttp.response;
				if (subject == null || subject === "") window.location.replace(domain + "/" + "error.html");

				let video = document.getElementById("video");
				if (video) video.src = "videos/" + subject + ".mp4"
			}
		};
		xhttp.send();
	}`

var selectionScript = `
	function selected(optionId) {
        let questionId = optionId.split("_")[0];
        document.getElementById(questionId + "_frame").style.borderStyle = "none";
        let xhttp = new XMLHttpRequest();
        xhttp.open("GET", domain + "/select?session_id=" + session_id + "&subject=" + subject + "&option=" + optionId);
        xhttp.send();
    }`

var endHTML = `</script>
</body>
</html>`


type Question struct {
	Body string
	Options []Option
}

type Option struct {
	Body string
	Type int
}

const (
	TypeCheckbox = iota
	TypeRadio
	TypeText
)

var optionTypes = map[int]string {
	TypeCheckbox: "checkbox",
	TypeRadio: "radio",
	TypeText: "text",
}
