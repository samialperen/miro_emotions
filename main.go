package main

import (
	"./database"
	"./server"
)

func main() {
	database.InitDB()
	server.Start()
}
