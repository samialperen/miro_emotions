package database

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/google/uuid"
	"strings"
	"sync"
)

var mu = new(sync.Mutex)
var db *sql.DB

func InitDB() {
	var err error
	db, err = sql.Open("mysql", "root:@/miro")
	db.SetMaxOpenConns(200)
	db.SetMaxIdleConns(200)
	panicError(err)
	createDB()
}

func createDB() {
	mu.Lock()
	defer mu.Unlock()

	var err error
	_, err = db.Exec(createUserStmt)
	panicError(err)
	_, err = db.Exec(createEmotionStmt)
	panicError(err)
	_, err = db.Exec(createDemographicStmt)
	panicError(err)
	_, err = db.Exec(createUnderstandingStmt)
	panicError(err)
	_, err = db.Exec(createIDAQStmt)
	panicError(err)
}

var createUserStmt = `CREATE TABLE IF NOT EXISTS user (
	session_id VARCHAR(32) NOT NULL,
	mturk_id VARCHAR(256) NOT NULL,
	hit_id VARCHAR(256) NOT NULL,
	current_emotion VARCHAR(256) NOT NULL,
	cannot_load INT NOT NULL DEFAULT 0,
	replay INT NOT NULL DEFAULT 0,
	play_comment INT NOT NULL DEFAULT 0,
	miro_gender VARCHAR(32) NOT NULL DEFAULT '',
	created TIMESTAMP NOT NULL,
	updated TIMESTAMP NOT NULL,
	UNIQUE KEY(session_id),
	PRIMARY KEY(mturk_id)
);`

var createEmotionStmt = `CREATE TABLE IF NOT EXISTS emotion (
	session_id VARCHAR(32) NOT NULL,
	emotion VARCHAR(256) NOT NULL,
	order_num INT NOT NULL,
	bored TINYINT(1) NOT NULL DEFAULT 0,
	surprise TINYINT(1) NOT NULL DEFAULT 0,
	disgust TINYINT(1) NOT NULL DEFAULT 0,
	tired TINYINT(1) NOT NULL DEFAULT 0,
	fear TINYINT(1) NOT NULL DEFAULT 0,
	excited TINYINT(1) NOT NULL DEFAULT 0,
	happy TINYINT(1) NOT NULL DEFAULT 0,
	annoyed TINYINT(1) NOT NULL DEFAULT 0,
	angry TINYINT(1) NOT NULL DEFAULT 0,
	calm TINYINT(1) NOT NULL DEFAULT 0,
	sad TINYINT(1) NOT NULL DEFAULT 0,
	not_sure TINYINT(1) NOT NULL DEFAULT 0,
	comment VARCHAR(1024) NOT NULL DEFAULT '',
	cannot_load INT NOT NULL DEFAULT 0,
	replay INT NOT NULL DEFAULT 0,
	play_comment INT NOT NULL DEFAULT 0,
	created TIMESTAMP NOT NULL,
	updated TIMESTAMP NOT NULL,
	PRIMARY KEY(session_id, emotion),
	FOREIGN KEY(session_id) REFERENCES user(session_id)
);`

var createDemographicStmt = `CREATE TABLE IF NOT EXISTS demographics (
	session_id VARCHAR(32) NOT NULL,
	age INT NOT NULL,
	gender VARCHAR(256) NOT NULL,
	education VARCHAR(256) NOT NULL,
	ethnicity VARCHAR(256) NOT NULL,
	miro_animal VARCHAR(256) NOT NULL,
	has_pet_self TINYINT(1) NOT NULL,
	pet_type_self VARCHAR(256) NOT NULL,
	has_pet_friend TINYINT(1) NOT NULL,
	pet_type_friend VARCHAR(256) NOT NULL,
	dog_cat INT NOT NULL DEFAULT -1,
	like_pet INT NOT NULL DEFAULT -1,
	scared INT NOT NULL DEFAULT -1,
	pet_emotion INT NOT NULL DEFAULT -1,
	pet_emotion_difficulty INT NOT NULL DEFAULT -1,
	animal_emotion INT NOT NULL DEFAULT -1,
	human_like INT NOT NULL DEFAULT -1,
	animal_like INT NOT NULL DEFAULT -1,
	machine_like INT NOT NULL DEFAULT -1,
	created TIMESTAMP NOT NULL,
	updated TIMESTAMP NOT NULL,
	PRIMARY KEY(session_id),
	FOREIGN KEY(session_id) REFERENCES user(session_id)
);`

var createUnderstandingStmt = `CREATE TABLE IF NOT EXISTS understanding (
	session_id VARCHAR(32) NOT NULL,
	picture VARCHAR(32) NOT NULL,
	order_num INT NOT NULL,
	angry TINYINT(1) NOT NULL DEFAULT 0,
	disgust TINYINT(1) NOT NULL DEFAULT 0,
	fear TINYINT(1) NOT NULL DEFAULT 0,
	happy TINYINT(1) NOT NULL DEFAULT 0,
	neutral TINYINT(1) NOT NULL DEFAULT 0,
	sad TINYINT(1) NOT NULL DEFAULT 0,
	surprise TINYINT(1) NOT NULL DEFAULT 0,
	not_sure TINYINT(1) NOT NULL DEFAULT 0,
	created TIMESTAMP NOT NULL,
	updated TIMESTAMP NOT NULL,
	PRIMARY KEY(session_id, picture),
	FOREIGN KEY(session_id) REFERENCES user(session_id)
);`

var createIDAQStmt = `CREATE TABLE IF NOT EXISTS idaq (
	session_id VARCHAR(32) NOT NULL,
	qq1 INT NOT NULL DEFAULT -1,
	qq2 INT NOT NULL DEFAULT -1,
	qq3 INT NOT NULL DEFAULT -1,
	qq4 INT NOT NULL DEFAULT -1,
	qq5 INT NOT NULL DEFAULT -1,
	qq6 INT NOT NULL DEFAULT -1,
	qq7 INT NOT NULL DEFAULT -1,
	qq8 INT NOT NULL DEFAULT -1,
	qq9 INT NOT NULL DEFAULT -1,
	qq10 INT NOT NULL DEFAULT -1,
	qq11 INT NOT NULL DEFAULT -1,
	qq12 INT NOT NULL DEFAULT -1,
	qq13 INT NOT NULL DEFAULT -1,
	qq14 INT NOT NULL DEFAULT -1,
	qq15 INT NOT NULL DEFAULT -1,
	qq16 INT NOT NULL DEFAULT -1,
	qq17 INT NOT NULL DEFAULT -1,
	qq18 INT NOT NULL DEFAULT -1,
	created TIMESTAMP NOT NULL,
	updated TIMESTAMP NOT NULL,
	PRIMARY KEY(session_id),
	FOREIGN KEY(session_id) REFERENCES user(session_id)
);`

func CreateUser(mturkId, hitId string) string {
	mu.Lock()
	defer mu.Unlock()

	getUserResult, err := db.Query(getUserStmt, mturkId)
	reportError(err)
	defer closeRows(getUserResult)

	oldSessionId := "error"

	if getUserResult.Next() {
		err = getUserResult.Scan(&oldSessionId)
		reportError(err)
		//return oldSessionId
		return "error" // reject re-entries
	}

	sessionId := newUUID()
	_, err = db.Exec(addUserStmt, sessionId, mturkId, hitId)
	reportError(err)

	return sessionId
}

var getUserStmt = `SELECT session_id FROM user WHERE mturk_id = ?;`

var addUserStmt = `INSERT INTO user (session_id, mturk_id, hit_id, current_emotion, created, updated) 
VALUES (?, ?, ?, '', NOW(), NOW());`

func newUUID() string {
	return strings.Replace(uuid.New().String(), "-", "", -1)
}

func GetSubject(sessionId string) string {
	fmt.Println("GetSubject", sessionId)
	mu.Lock()
	defer mu.Unlock()

	getSubjectResult, err := db.Query(getSubjectStmt, sessionId)
	reportError(err)
	defer closeRows(getSubjectResult)

	if getSubjectResult.Next() {
		subject := ""
		err = getSubjectResult.Scan(&subject)
		reportError(err)
		return subject
	}

	return ""
}

var getSubjectStmt = `SELECT current_emotion FROM user WHERE session_id = ?;`

func GetCompletedSubjects(sessionId string) map[string]bool {
	fmt.Println("GetCompletedSubjects", sessionId)
	mu.Lock()
	defer mu.Unlock()

	getCompletedSubjectsResult, err := db.Query(getCompletedSubjectsStmt, sessionId)
	reportError(err)
	defer closeRows(getCompletedSubjectsResult)

	result := make(map[string]bool)

	for getCompletedSubjectsResult.Next() {
		subject := ""
		err = getCompletedSubjectsResult.Scan(&subject)
		reportError(err)
		result[subject] = true
	}

	return result
}

var getCompletedSubjectsStmt = `SELECT emotion FROM emotion WHERE session_id = ?;`

func UpdateSubject(sessionId, subject string) {
	fmt.Println("UpdateSubject", sessionId, subject)
	mu.Lock()
	defer mu.Unlock()

	_, err := db.Exec(updateSubjectStmt, subject, sessionId)
	reportError(err)
}

var updateSubjectStmt = `UPDATE user SET current_emotion = ? WHERE session_id = ?;`

func EntryExists(sessionId, subject string) bool {
	fmt.Println("EntryExists", sessionId, subject)
	mu.Lock()
	defer mu.Unlock()

	fmt.Println("exists", sessionId, subject)

	getEntryResult, err := db.Query(getEntryStmt, sessionId, subject)
	defer closeRows(getEntryResult)

	reportError(err)
	return getEntryResult.Next()
}

var getEntryStmt = `SELECT * FROM emotion WHERE session_id = ? AND emotion = ?;`

func CreateEntry(sessionId, subject string) {
	fmt.Println("CreateEntry", sessionId, subject)
	mu.Lock()
	defer mu.Unlock()

	countEntryResult, err := db.Query(countEntryStmt, sessionId)
	defer closeRows(countEntryResult)
	entryCount := 0
	reportError(err)
	if countEntryResult.Next() {
		err = countEntryResult.Scan(&entryCount)
	}

	fmt.Println("create", sessionId, subject, entryCount + 1)

	_, err = db.Exec(createEntryStmt, sessionId, subject, entryCount + 1)
	reportError(err)
}

var countEntryStmt = `SELECT COUNT(*) FROM emotion WHERE session_id = ?;`

var createEntryStmt = `INSERT INTO emotion (session_id, emotion, order_num, created, updated) VALUES
(?, ?, ?, NOW(), NOW());`

func UpdateEntry(sessionId, subject string, options map[string]bool) {
	fmt.Println("UpdateEntry", sessionId, subject)
	mu.Lock()
	defer mu.Unlock()

	_, err := db.Exec(updateEntryStmt, options["bored"], options["surprise"], options["disgust"],
		options["tired"], options["fear"], options["excited"], options["happy"], options["annoyed"],
		options["angry"], options["calm"], options["sad"], options["not_sure"],
		sessionId, subject)
	reportError(err)
}

var updateEntryStmt = `UPDATE emotion SET 
                   bored = ?, surprise = ?, disgust = ?, tired = ?,
                   fear = ?, excited = ?, happy = ?, annoyed = ?, angry = ?, calm = ?,
                   sad = ?, not_sure = ?, updated = NOW()
WHERE session_id = ? AND emotion = ?;`

func UpdateComment(sessionId, subject, comment string) {
	fmt.Println("UpdateComment", sessionId, subject, comment)
	mu.Lock()
	defer mu.Unlock()

	_, err := db.Exec(updateCommentStmt, comment, sessionId, subject)
	reportError(err)
}

var updateCommentStmt = `UPDATE emotion SET comment = ?, updated = NOW() WHERE session_id = ? AND emotion = ?;`

func CreateUnderstanding(sessionId, picture string, options map[string]bool) {
	fmt.Println("CreateUnderstanding", sessionId)
	mu.Lock()
	defer mu.Unlock()

	completedPictures := GetCompletedPictures(sessionId)
	orderNum := len(completedPictures)
	fmt.Println("what1")
	_, err := db.Exec(insertUnderstandingStmt, sessionId, picture, orderNum, options["angry"],
		options["disgust"], options["fear"], options["happy"], options["neutral"], options["sad"],
		options["surprise"], options["not_sure"])
	fmt.Println("what2")
	reportError(err)
}

var insertUnderstandingStmt = `INSERT INTO understanding (
	session_id, picture, order_num, angry, disgust, fear, happy, neutral, sad, surprise, not_sure, created, updated
) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), NOW());`

func CannotPlay(sessionId, subject string) {
	mu.Lock()
	defer mu.Unlock()

	cannotPlay := CountCannotPlay(sessionId)

	_, err := db.Exec(updateCannotPlayStmt, cannotPlay + 1, sessionId)
	reportError(err)

	_, err = db.Exec(labelCannotPlayStmt, sessionId, subject)
	reportError(err)
}

func CountCannotPlay(sessionId string) int {
	countCannotPlayResult, err := db.Query(countCannotPlayStmt, sessionId)
	reportError(err)
	defer closeRows(countCannotPlayResult)

	cannotPlay := 0
	if countCannotPlayResult.Next() {
		err = countCannotPlayResult.Scan(&cannotPlay)
		reportError(err)
	}

	return cannotPlay
}

var countCannotPlayStmt = `SELECT cannot_load FROM user WHERE session_id = ?;`
var updateCannotPlayStmt = `UPDATE user SET cannot_load = ? WHERE session_id = ?;`
var labelCannotPlayStmt = `UPDATE emotion SET cannot_load = 1 WHERE session_id = ? AND emotion = ?;`

func CreateDemographics(sessionId string, optionsStr map[string]string, optionsInt map[string]int,
	optionBool map[string]bool) {
	fmt.Println("CreateDemographics", sessionId)
	mu.Lock()
	defer mu.Unlock()

	_, err := db.Exec(insertDemographicsStmt, sessionId, optionsInt["age"], optionsStr["gender"],
		optionsStr["education"], optionsStr["ethnicity"], optionsStr["miro_animal"],
		optionBool["has_pet_self"], optionsStr["pet_type_self"], optionBool["has_pet_friend"],
		optionsStr["pet_type_friend"], optionsInt["dog_cat"], optionsInt["like_pet"],
		optionsInt["scared"], optionsInt["pet_emotion"], optionsInt["pet_emotion_difficulty"],
		optionsInt["animal_emotion"], optionsInt["human_like"], optionsInt["animal_like"],
		optionsInt["machine_like"])
	reportError(err)
}

var insertDemographicsStmt = `INSERT INTO demographics (
	session_id, age, gender, education, ethnicity, miro_animal, has_pet_self, pet_type_self,
	has_pet_friend, pet_type_friend, dog_cat, like_pet, scared, pet_emotion, pet_emotion_difficulty,
	animal_emotion, human_like, animal_like, machine_like, created, updated
) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), NOW());`

func CreateIdaq(sessionId string, options map[string]int) {
	fmt.Println("CreateIdaq", sessionId)
	mu.Lock()
	defer mu.Unlock()

	_, err := db.Exec(insertIdaqStmt, sessionId,
		options["qq1"], options["qq2"], options["qq3"], options["qq4"], options["qq5"],
		options["qq6"], options["qq7"], options["qq8"], options["qq9"], options["qq10"],
		options["qq11"], options["qq12"], options["qq13"], options["qq14"], options["qq15"],
		options["qq16"], options["qq17"], options["qq18"])
	reportError(err)
}

var insertIdaqStmt = `INSERT INTO idaq (
	session_id, qq1, qq2, qq3, qq4, qq5, qq6, qq7, qq8, qq9, qq10, qq11, qq12, qq13, qq14, qq15,
    qq16, qq17, qq18, created, updated
) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), NOW());`

func AddReplay(sessionId, subject string) {
	mu.Lock()
	defer mu.Unlock()

	replay := CountReplay(sessionId)

	_, err := db.Exec(updateReplayStmt, replay + 1, sessionId)
	reportError(err)

	replay = CountEmotionReplay(sessionId, subject)

	_, err = db.Exec(updateEmotionReplayStmt, replay + 1, sessionId, subject)
	reportError(err)
}

func CountReplay(sessionId string) int {
	countReplayResult, err := db.Query(countReplayStmt, sessionId)
	reportError(err)
	defer closeRows(countReplayResult)

	replay := 0
	if countReplayResult.Next() {
		err = countReplayResult.Scan(&replay)
		reportError(err)
	}

	return replay
}

func CountEmotionReplay(sessionId, subject string) int {
	countEmotionReplayResult, err := db.Query(countEmotionReplayStmt, sessionId, subject)
	reportError(err)
	defer closeRows(countEmotionReplayResult)

	replay := 0
	if countEmotionReplayResult.Next() {
		err = countEmotionReplayResult.Scan(&replay)
		reportError(err)
	}

	return replay
}

var countReplayStmt = `SELECT replay FROM user WHERE session_id = ?;`
var updateReplayStmt = `UPDATE user SET replay = ? WHERE session_id = ?;`
var countEmotionReplayStmt = `SELECT replay FROM emotion WHERE session_id = ? AND emotion = ?;`
var updateEmotionReplayStmt = `UPDATE emotion SET replay = ? WHERE session_id = ? AND emotion = ?;`

func AddWatchAgain(sessionId, subject string) {
	mu.Lock()
	defer mu.Unlock()

	watchAgain := CountWatchAgain(sessionId)

	_, err := db.Exec(updateWatchAgainStmt, watchAgain + 1, sessionId)
	reportError(err)

	watchAgain = CountEmotionWatchAgain(sessionId, subject)

	_, err = db.Exec(updateEmotionWatchAgainStmt, watchAgain + 1, sessionId, subject)
	reportError(err)
}

func CountWatchAgain(sessionId string) int {
	countWatchAgainResult, err := db.Query(countWatchAgainStmt, sessionId)
	reportError(err)
	defer closeRows(countWatchAgainResult)

	watchAgain := 0
	if countWatchAgainResult.Next() {
		err = countWatchAgainResult.Scan(&watchAgain)
		reportError(err)
	}

	return watchAgain
}

func CountEmotionWatchAgain(sessionId, subject string) int {
	countEmotionWatchAgainResult, err := db.Query(countEmotionWatchAgainStmt, sessionId, subject)
	reportError(err)
	defer closeRows(countEmotionWatchAgainResult)

	watchAgain := 0
	if countEmotionWatchAgainResult.Next() {
		err = countEmotionWatchAgainResult.Scan(&watchAgain)
		reportError(err)
	}

	return watchAgain
}

var countWatchAgainStmt = `SELECT play_comment FROM user WHERE session_id = ?;`
var updateWatchAgainStmt = `UPDATE user SET play_comment = ? WHERE session_id = ?;`
var countEmotionWatchAgainStmt = `SELECT play_comment FROM emotion WHERE session_id = ? AND emotion = ?;`
var updateEmotionWatchAgainStmt = `UPDATE emotion SET play_comment = ? WHERE session_id = ? AND emotion = ?;`

func GetCompletedPictures(sessionId string) map[string]bool {
	fmt.Println("GetCompletedPictures", sessionId)

	getCompletedPicturesResult, err := db.Query(getCompletedPicturesStmt, sessionId)
	reportError(err)
	defer closeRows(getCompletedPicturesResult)

	result := make(map[string]bool)

	for getCompletedPicturesResult.Next() {
		subject := ""
		err = getCompletedPicturesResult.Scan(&subject)
		reportError(err)
		result[subject] = true
	}

	return result
}

var getCompletedPicturesStmt = `SELECT picture FROM understanding WHERE session_id = ?;`

func UpdateGender(sessionId, gender string) {
	mu.Lock()
	defer mu.Unlock()

	_, err := db.Exec(updateGenderStmt, gender, sessionId)
	reportError(err)
}

var updateGenderStmt = `UPDATE user SET miro_gender = ? WHERE session_id = ?;`

func closeRows(result *sql.Rows) {
	err := result.Close()
	reportError(err)
}

func reportError(err error) {
	if err != nil {
		fmt.Println(err)
	}
}

func panicError(err error) {
	if err != nil {
		panic(err)
	}
}
